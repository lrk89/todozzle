'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'views/home/home.html',
    controller: 'homeCtrl'
  });
}]).controller('homeCtrl', ['$scope', 'todoAPI', function($scope, todoAPI) {


//Get all todo's on load

$scope.backlog = {};
  $scope.todos = todoAPI.query();

 $scope.view = "home";

 //set defaults

 /*moving shit */
 $scope.onDragComplete=function(data,evt){
     console.log("drag success, data:", data);
  }
  $scope.onDropComplete=function(data,evt){
      console.log("drop success, data:", data);
  }
/*end moving shit */
//Submit a todo
 $scope.addTodo = function() {


   /*
   time
   1w = 5 days
   1day = 6 hours

If you do not specify a time unit, minute will be assumed.
*/
   //save Todo
   todoAPI.create($scope.newTodo)

   //reload Todo List
  $scope.todos = todoAPI.query();



  }

  $scope.selectTodo = function(todo) {
    //save Todo

    //reload Todo List
   $scope.selectedTodo = todo;

   }
}]);
