angular.module('myApp.todoAPI', ['ngResource'])

  .factory('todoAPI', function($resource) {
    return $resource("/api/todos/:id", {id: '@id'}, {
        get: { method: "GET", isArray: false, params: { id: 0 }},
//      update: { method: "PUT"},
      create: {method: 'POST'}
    });

  });
