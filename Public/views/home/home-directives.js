var App = angular.module('myApp.directives', [])
.filter('isAfter', function() {
  return function(items, dateAfter) {
    // Using ES6 filter method


        return items.filter(function(item){
          var weekBeforeLast =  moment().subtract(2, 'week').format('MM/DD/YYYY');
          var currentDate =  moment().subtract(0, 'week').format('MM/DD/YYYY');


          return moment(item.created).isAfter(weekBeforeLast) &&  moment(item.created).isBefore(currentDate);
        })
  }
})
.filter('isCurrent', function() {
  return function(items) {
    // Using ES6 filter method
    return items.filter(function(item){
      var lastWeek =  moment().subtract(1, 'week').format('MM/DD/YYYY');
        var currentDate =  moment().subtract(0, 'week').format('MM/DD/YYYY');

      return moment(item.created).isAfter(lastWeek) && moment(item.created).isBefore(currentDate);
    })
  }
})
