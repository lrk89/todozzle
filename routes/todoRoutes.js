var express = require('express');

var routes = function(todo) {


  var todoRouter = express.Router();

var todoController = require('../controllers/todoController')(todo);
  todoRouter.route('/')
  .post(todoController.post)
  .get(todoController.get)

todoRouter.use('/:todoId', function(req,res,next) {
  todo.findById(req.params.todoId, function(err, todo) {
    if(err) {
      res.status(500).send(err);
    }
    else if (todo) {
      req.todo = todo;
      next();
    }
    else{
      res.status(404).send('no todo found');
    }

  });
});
  todoRouter.route('/:todoId')
  .get(function(req, res) {
  //get all
      res.json(req.todo);
  })
  .post(function(req, res) {

        req.todo.title = req.body.title;
        req.todo.description = req.body.description;
        req.todo.category = req.body.category;
        req.todo.status = req.body.status;
        req.todo.progress = req.body.progress;
        req.todo.done = req.body.done;
        req.todo.created = req.body.created;
        req.todo.creator = req.body.creator;
        req.todo.assignee = req.body.assignee;
        req.todo.estimation = req.body.estimation;
        req.todo.timeSpent = req.body.timeSpent;

        if(err) {
          res.status(500).send(err);
        }
        else {
          res.json(req.todo);
        }
        res.json(todo);

  })
  .patch(function(req,res) {

    if (req.body._id) {
      delete req.body._id;
}
  for (var p in req.body) {
    req.todo[p] = req.body[p];
  }

  req.todo.save(function(err) {
    if(err) {
      res.status(500).send(err);
    }
    else {
      console.log(req.todo)
      res.json(req.todo);
    }
  });
  })
  .delete(function(req, res) {
    req.todo.remove(function(err) {
      if(err) {
        res.status(500).send(err);
      }
      else {
        res.status(204).send('Removed todo');
      }
    });
  })
return todoRouter;
};

module.exports = routes;
