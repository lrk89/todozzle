// Load Required Modules
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');

//Database
var db = mongoose.connect('mongodb://localhost/todozzle');

//Model
var todo = require('./models/todoModel');

var app = express();
var port = process.env.PORT || 1337;

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'Public/Views')));
app.use(express.static(path.join(__dirname, 'Public/UI/css')));
app.use(express.static(path.join(__dirname, 'Public/UI/js')));
app.use(express.static(path.join(__dirname, 'Public/UI')));
app.use(express.static(path.join(__dirname, 'Public')));

todoRouter = require('./Routes/todoRoutes')(todo);

app.use('/api/todos', todoRouter);

app.get('/', function(req, res){
    res.render('index.html');
});


app.listen(port, function() {
  console.log("running on: " + port);
});
