var todoController = function(todo) {

  var post = function(req, res) {

  var newtodo = new todo(req.body);
 if (!req.body.title) {
   res.status(400);
   res.send('Title is required');
 }
 else {
  newtodo.save();
  res.status(201);
  res.send(newtodo);
}
};

var get = function(req, res) {


  //get all
    todo.find(function(err, todos) {
      if(err) {
        res.status(500).send(err);
      }
      else {
        var returntodos = [];

        todos.forEach(function(element, index, array) {
         var newtodo = element.toJSON();
         newtodo.links = {};
         newtodo.links.self = 'http://' + req.headers.host + '/api/todos/' + newtodo._id;
         returntodos.push(newtodo);
       });

          res.json(returntodos);


      }

    });

};

return {
  post:post,
  get:get
};
};
module.exports = todoController;
