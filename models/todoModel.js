var mongoose = require('mongoose');
var schema = mongoose.Schema;

var todoModel = new schema({
  title: {
    type: String
  },
  description: {
    type: String
  },
  category: {
    type: String, default:"Task"
  },
  status: {
    type: String, default:"Minor"
  },
  progress: {
    type: String, default: "backlog"
  },
  done: {
    type: Boolean, default:false
  },
  created: {
  type: Date, default: Date.now
  },
  creator: {
    type: String
  },
  assignee: {
    type: String
  },
  estimation: {
    type: String
  },
  timeSpent: {
    type: Number, default: 0
  }
});

module.exports = mongoose.model('todo', todoModel);
